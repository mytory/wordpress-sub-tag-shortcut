import { registerFormatType, unregisterFormatType } from '@wordpress/rich-text';
import { subscript } from './subscript';

unregisterFormatType('core/subscript');
registerFormatType(subscript.name, subscript);
