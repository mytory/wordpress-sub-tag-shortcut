=== Sub Tag Shortcut ===
Contributors: mytory
Tags: editor, formatting, text, subscript
Requires at least: 5.0
Tested up to: 6.4.2
Stable tag: 1.0.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Author: mytory
Author URI: https://mytory.net

A simple WordPress plugin to add a subscript text formatting shortcut(Ctrl+Alt+S(Ctrl+Otp+S for Mac)) to the block editor.

== Description ==

The Sub Tag Shortcut plugin adds a subscript formatting option to the WordPress block editor. Users can apply subscript formatting to their text using a simple keyboard shortcut (Ctrl+Alt+S).

== Installation ==

1. Upload the `sub-tag-shortcut` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Use the shortcut in the block editor to apply subscript formatting.

== Frequently Asked Questions ==

= What is the keyboard shortcut for subscript formatting? =

You can use Ctrl+Alt+S(Ctrl+Otp+S for Mac) to apply subscript formatting to your selected text in the block editor.

= Does this plugin support other editors? =

Sub Tag Shortcut only supports the WordPress block editor.

== Changelog ==

= 1.0.0 =
* Initial release.

== Upgrade Notice ==

= 1.0.0 =
Initial release.
