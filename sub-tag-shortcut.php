<?php
/**
 * Plugin Name: Sub Tag Shortcut
 * Plugin URI:  https://mytory.net
 * Description: Adds subscript formatting shortcut to the WordPress block editor.
 * Version:     1.0.0
 * Author:      mytory
 * Author URI:  https://mytory.net
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: sub-tag-shortcut
 */

// 여기에 필요한 PHP 코드를 추가합니다.

// 워드프레스에 스크립트 등록
function sub_tag_shortcut_enqueue_scripts() {
    wp_enqueue_script(
        'sub-tag-shortcut-script',
        plugins_url('build/index.js', __FILE__),
        array('wp-rich-text', 'wp-element', 'wp-block-editor', 'wp-data'),
        '1.0.0',
        true
    );
}

add_action('enqueue_block_editor_assets', 'sub_tag_shortcut_enqueue_scripts');
