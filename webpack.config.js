const path = require('path');
const defaultConfig = require("@wordpress/scripts/config/webpack.config");

module.exports = {
    ...defaultConfig,
    entry: {
        index: './index.js'
    },
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'index.js'
    }
};
