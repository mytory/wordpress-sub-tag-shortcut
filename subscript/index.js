/**
 * sub 태그에 단축키 ctrl+alt+s를 매기는 플러그인
 * 아래 두 링크 참고함.
 * https://github.com/WordPress/gutenberg/blob/trunk/packages/format-library/src/subscript/index.js
 * https://github.com/WordPress/gutenberg/blob/trunk/packages/format-library/src/bold/index.js
 */
import { __ } from '@wordpress/i18n';
import { toggleFormat } from '@wordpress/rich-text';
import {
    RichTextToolbarButton,
    RichTextShortcut,
    __unstableRichTextInputEvent,
} from '@wordpress/block-editor';
import { subscript as subscriptIcon } from '@wordpress/icons';

const name = 'core/subscript';
const title = __('Subscript');

export const subscript = {
    name,
    title,
    tagName: 'sub',
    className: null,
    edit({ isActive, value, onChange, onFocus }) {
        function onToggle() {
            onChange(toggleFormat(value, { type: name, title }));
        }

        function onClick() {
            onToggle();
            onFocus();
        }

        return (
            <>
                <RichTextShortcut
                    type="access" // modifier 키 코드는 다음을 참고: https://github.com/WordPress/gutenberg/blob/trunk/packages/keycodes/src/index.js#L182
                    character="s"
                    onUse={onToggle}
                />
                <RichTextToolbarButton
                    icon={subscriptIcon}  // 아이콘
                    title={title}
                    onClick={onClick}
                    isActive={isActive}
                    shortcutType="access"
                    shortcutCharacter="s"
                />
                <__unstableRichTextInputEvent
                    inputType="formatSubscript"  // 입력 이벤트 타입
                    onInput={onToggle}
                />
            </>
        );
    },
};
